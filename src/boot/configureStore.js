import MainStore from "../store/DomainStore/HomeStore";
import LoginStore from "../store/ViewStore/LoginViewStore";

import TodosStore from "../store/FirebaseStore/TodosStore";

export default function() {
	const mainStore = MainStore;
	const loginForm = LoginStore;

	const todosStore = TodosStore;

	return {
		loginForm,
		mainStore,
		todosStore				
	};
}
