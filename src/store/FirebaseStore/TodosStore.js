import { types } from "mobx-state-tree";
import firebase from "react-native-firebase";

const todosRef = firebase.firestore().collection("todos");

const Todos = types
  .model("Todos", {
    items: types.optional(types.frozen)
  })
  .actions(self => ({
    addTodo(title, description) {
      return todosRef.add({
        title: title,
        description: description,
        is_completed: false
      });
    },
    getTodos() {
      return todosRef.get();
    }
  }));

const TodosStore = Todos.create({});

export default TodosStore;
