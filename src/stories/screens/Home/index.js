import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Item,
  Input,
  Textarea,
  Text
} from "native-base";

import styles from "./styles";

class Home extends React.Component {
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Home</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Item
            regular
            style={{ marginBottom: 10 }}
          >
            <Input
              placeholder="Title"
              onChangeText={text => this.props.onChangeTitle(text) }/>
          </Item>
          <Textarea
            bordered
            rowSpan={5}
            placeholder="Description"
            onChangeText={text => this.props.onChangeDescription(text) }
            style={{
              fontSize: 17,
              marginBottom: 10
            }}
          />
          <Button onPress={() => this.props.onSubmit()}>
            <Text>Submit</Text>
          </Button>
          <Button onPress={() => this.props.onGet()}>
            <Text>Get Todos</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default Home;
