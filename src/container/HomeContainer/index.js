
import * as React from "react";
import { Toast } from "native-base";
import { observer, inject } from "mobx-react/native";

import Home from "../../stories/screens/Home";

@inject("mainStore", "todosStore")
@observer
export default class HomeContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: "",
			description: "",
			isCompleted: false
		};
	}

	validate() {
		const { title, description } = this.state;

		if (title === "") {
			return false;
		}
		if (description === "") {
			return false;
		}

		return true;
	}

	addTodo() {
		const { todosStore } = this.props;
		const { title, description } =  this.state;

		if (!this.validate()) {
			Toast.show({
				type: "danger",
				text: "Check your input.",
				buttonText: "Okay"
			});

			return;
		}

		todosStore
			.addTodo(title, description)
			.then((doc) => {
				Toast.show({
					text: "Todo is added.",
					buttonText: "Okay"
				});
			})
			.catch((err) => {
				Toast.show({
					text: err.toString(),
					buttonText: "Okay"
				});
			});
	}

	getTodos() {
		const { todosStore } = this.props;

		todosStore
			.getTodos()
			.then((snapshot) => {
				snapshot.forEach(function(doc) {
					console.log(doc.id, " => ", doc.data());
				});
			});
	}

	onChangeTitle(text) {
		this.setState({ title: text });
	}

	onChangeDescription(text) {
		this.setState({ description: text });
	}

	render() {
		return (
			<Home
				onGet={() => this.getTodos()}
				onSubmit={() => this.addTodo()}
				onChangeTitle={text => this.onChangeTitle(text)}
				onChangeDescription={text => this.onChangeDescription(text)}
				navigation={this.props.navigation}
			/>
		);
	}
}
